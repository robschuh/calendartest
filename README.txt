

This code has been build only for a test purpose.

To run it please open your shell and pass two params (integers) to the script "month" and "year".
ejm > php calendar.php 3 2017  (this is the script start month and the year). 

NOTE: Arguments are optional, if you do not write any argument the start month is the current month (same for the year).
ejm > php calendar.php

NOTE: normally I use a FWs like laravel to do this common tasks, and composer to autoload classes, but for this exercise I did not use any of them. I used namespaces for all my classes/entities.
