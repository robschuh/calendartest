<?php

namespace Test\Entities;

abstract class File {


	public function __construct(array $lines) {

		$this->save($lines);
	}
	
	public abstract function save(array $lines);

}
