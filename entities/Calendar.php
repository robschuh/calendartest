<?php

namespace Test\Entities;
use DateTime;

class calendar {

	// These attributes could be PRIVATE, but I decided to put them PROTECTED because a possible child class could be inherit.
	protected $currentMonth;
	protected $currentYear;
	protected $lastMonthDay;
	protected $lastMonthWeekDay;
	protected $lastWorkingDay;
	protected $bonusWeekDay;
	protected $stringDays = array('Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

	const BONUS_DAY = 12;
    const CAL_GREGORIAN = 1;

	public function __construct ($currentMonth, $currentYear) {
		$this->currentMonth = $currentMonth;
		$this->currentYear = $currentYear;
	}


	/*
	* Get the month total days / last month day.
	*/
	protected function totalMonthDays() {
		$this->lastMonthDay = cal_days_in_month(self::CAL_GREGORIAN, $this->currentMonth, $this->currentYear); // 31
		return $this->lastMonthDay;

	}

	/*
	* Get the last week day of the month
	*/
	protected function lastWeekDayOfTheMonth() {
		$this->lastMonthWeekDay = date('w', strtotime($this->currentYear.'-' .$this->currentMonth. '-' .$this->lastMonthDay));
		
		return  $this->stringDays[$this->lastMonthWeekDay ];
	}

	/*
	* Get the last working day of the month
	*/
	protected function lastWorkingDay() {

		// NOTE: Monday is position cero .
		$date = $this->checkCloseWorkingDay($this->lastMonthWeekDay);
		$this->lastWorkingDay = $date['weekDay'];
		return $date['monthDay']. '-'.$this->stringDays[$this->lastWorkingDay];
	}

	/*
	* Get the last week day for bonus payment.
	*/
	protected function bonusPaymentDay() {

		$date = $this->checkCloseWorkingBonusDay(date('w', strtotime($this->currentYear.'-' .$this->currentMonth. '-' .self::BONUS_DAY)));
		
		$this->bonusWeekDay = $date['weekDay'];
		return $date['monthDay']. '-'.$this->stringDays[$this->bonusWeekDay];
	}

	/*
	* Return an array with Basic pay week day and the month day.
	*/
	protected function checkCloseWorkingDay ($weekDay) {

		if ( $weekDay == 6 ) {
			$date = array('weekDay' => 5, 'monthDay' => $this->lastMonthDay - 1);
		}
		else if ( $weekDay == 0 ){
			$date = array('weekDay' => 1, 'monthDay' =>  1);
		}else{
			$date = array('weekDay' => $weekDay, 'monthDay' => $this->lastMonthDay);
		}

		return $date;		
	}

	/*
	* Return an array with Bonus day week day and the month day.
	* $weekDay = the 12 day of the current month.
	*/
	protected function checkCloseWorkingBonusDay ($weekDay) {
	
		$date = array();
		
		// If the last week day is saturday we get the next tuesday (2) and the monthDay is 3.
		if ( $weekDay == 6 ) {
		
			$date['monthDay'] = self::BONUS_DAY + 3;
			$date['weekDay'] = 2;
		}

		// If the last week day is sunday we get the next tuesday (2) and the monthDay is 2.
		else if ( $weekDay == 0 ){
			$date['monthDay'] = self::BONUS_DAY + 2;
			$date['weekDay'] = 2;

		// If the last week day is a working day we check which day it is not tuesday take the next tuesday.	
		}else{
			$date['weekDay'] = $weekDay;
			$nextDays = 7 - $weekDay;
			$date['monthDay'] = self::BONUS_DAY; 
		}
	 
		return $date;		
	}

	
	// Get the month name (string).
	protected function getMonthName($monthInteger) {
		$dateObj   = DateTime::createFromFormat('!m', $monthInteger);
		return $dateObj->format('F'); // March
	}

	/*
	* Call all the object function to create the CSV calendar file.
	*/
	public static function createCalendar ($argv = NULL){

		$data = array();
		$currentMonth = (isset($argv[1]) ? $argv[1] : date('m'));
		$currentYear =  (isset($argv[2]) ? $argv[2] : date('Y'));;

		for ($i = 1; $i <= 12; $i++) {

			$calendar = new Calendar($currentMonth, $currentYear);
			$data[$currentMonth]['month'] = $calendar->getMonthName($calendar->currentMonth) .' '.$calendar->currentYear;
			$calendar->totalMonthDays();
			$calendar->lastWeekDayOfTheMonth();
			$data[$currentMonth]['lastWorkingDay'] = $calendar->lastWorkingDay();
			$data[$currentMonth]['bonusPaymentDay'] = $calendar->bonusPaymentDay();
			
			if ($currentMonth == 12 ) {
				$currentMonth = 1;
				$currentYear++;
				$calendar->currentMonth = $currentMonth;
				$calendar->currentYear = $currentYear;
			} else {
				$currentMonth++; 
				$calendar->currentMonth = $currentMonth;
			}

			unset($calendar);
		}

		// Save calendar data in a CSV file.
		$file = new FileCsv($data);
	}
}
