<?php

namespace Test\Entities;

require('File.php');

class FileCsv extends File{
	

	public function save(array $lines) {
		$this->generateCsv($lines);
	}

	// Write into the CSV file, if it not exists the method create the file.
	private function generateCsv($data) {

		$output = fopen("calendar.csv","w+") or die("Can't open php://output");

		header("Content-Type:application/csv"); 
	 	header('Content-Disposition: attachment; filename="calendar.csv"');

		fputcsv($output, array('Month', 'Working payment day', 'Bonus payment day'));
		foreach($data as $row) {

			fputcsv($output, $row);
		}
		fclose($output) or die("Can't close php://output");
	}
}